// https://nuxt.com/docs/api/configuration/nuxt-config
import { resolve } from "path"
export default defineNuxtConfig({
  devtools: { enabled: true },
  modules: [
    '@pinia/nuxt',
    '@element-plus/nuxt',
    '@nuxtjs/i18n',
    ],
  alias: {
    '@': resolve(__dirname, "/src"),
  },
  postcss: {
    plugins: {
      tailwindcss: {},
      autoprefixer: {},
    },
  },
  plugins: [
    '~/plugins/repository.ts',
    '~/plugins/axios',
    '~/plugins/toastBug',
  ],
  css: [
    "~/assets/style/main.scss",
    '~/assets/style/main.css',
    '~/assets/fonts/helvetica.css'
  ],
  i18n: {
    lazy: true,
    langDir: "locales",
    strategy: 'prefix_except_default',
    locales: [
      {
        code: 'VN',
        name: 'Vietnamese (VIE)',
        file: 'vi.json'
      },
      {
        code: 'en-US',
        name: 'English (UK)',
        file: 'en.json'
      },
    ],
    defaultLocale: 'VN',
  }
})
