import en from "~/locales/en.json"
import vi from "~/locales/vi.json"
export default defineI18nConfig(() => ({
    lazy: true,
    langDir: "locales",
    strategy: 'prefix_except_default',
    legacy: false,
    defaultlocale: 'VN',
    locales: [
        {
            code: 'VN',
            name: 'Vietnamese (VIE)',
            file: vi
        },
        {
            code: 'en-US',
            name: 'English (UK)',
            file: en
        },
    ],
    
  }))