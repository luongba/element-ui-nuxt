import { useI18n } from 'vue-i18n'
import { defineStore } from "pinia";
import nuxtStorage from 'nuxt-storage';
export const useNavbarStore = defineStore('navbar', ()=> {
    const {locale} = useI18n();
    const langLocal:any =  nuxtStorage.localStorage.getData('lang') || 'vi'
    const isCollapse = ref<boolean>(false);
    const changeStatusCollapse = () => {
        isCollapse.value = !isCollapse.value
    }

    const changeLanguage = (lang: string = langLocal) => {
        nuxtStorage.localStorage.setData('lang', lang);
        locale.value = langLocal
    }

    return { isCollapse , changeStatusCollapse, changeLanguage}
})