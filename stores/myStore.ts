import { defineStore } from "pinia";

export const useCounterStore = defineStore('counter', ()=> {
    const count = ref<number>(1);

    const increment = () => {
        count.value = count.value + 1
    }

    return { count , increment}
})