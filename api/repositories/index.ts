import LessonRepository from './lesson/lessonRepository'

export default (axios:any) => {
  return {
    lesson: LessonRepository(axios),
  }
}
