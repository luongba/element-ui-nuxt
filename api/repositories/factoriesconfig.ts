export default (axios:any) => (resource: string) => {
  return {
    all(config = {}) {
      return axios.get(`${resource}`, config)
    },
  
    index(config = {}) {
      return axios.get(`${resource}`, config)
    },
  
    create(payload: string, config = {}) {
      return axios.post(`${resource}`, payload, config)
    },
  
    action(id:string, action:any, payload: string, config = {}) {
      return axios.post(`${resource}/${id}/${action}/`, payload, config)
    },
  
    show(id:string, config = {}) {
      return axios.get(`${resource}/${id}/`, config)
    },
  
    delete(id: string, config = {}) {
      return axios.delete(`${resource}/${id}`, config)
    },
  }
}
