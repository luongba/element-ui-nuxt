import factory from '../factoriesconfig'
const resource = '/hack/'
export default (axios:any) => {
    return factory(axios)(resource)
}