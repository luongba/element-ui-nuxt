import { ElNotification } from '~/.nuxt/imports'
export default defineNuxtPlugin((ctx) => {
  return {
      provide: {
        toastBug: (objectError: any) => {
            const errorCode = objectError?.response?.status
  if (errorCode < 500) {
    if (errorCode === 400) {
      //  loi 400
      const error = objectError?.response?.data?.detail
    } else if (errorCode === 404) {
      // loi 404
      ElNotification.error({
        message: 'Không tìm thấy',
        duration: 2000,
        position: 'bottom-right',
      })
    } else {
      //  loi 4xx con lai
      try {
        const object = objectError?.response?.data?.detail
        if (typeof object === 'object') {
          ElNotification.error({
            message: 'Thông tin đầu vào không hợp lệ',
            duration: 2000,
            position: 'bottom-right',
          })
        } else
          ElNotification.error({
            message: object,
            duration: 2000,
            position: 'bottom-right',
          })
      } catch (e) {
        ElNotification.error({
          message: 'Có lỗi xảy ra',
          duration: 2000,
          position: 'bottom-right',
        })
      }
    }
  } else {
    // loi 5xx
    ElNotification.error({
      message: 'Lỗi không xác định',
      duration: 2000,
      position: 'bottom-right',
    })
  }
        }
      }
    }
  })