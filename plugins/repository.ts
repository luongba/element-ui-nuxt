import createRepository from '~/api/repositories'
import axios from 'axios'
export default defineNuxtPlugin((ctx) => {
  console.log("🚀 ~ file: repository.ts:4 ~ defineNuxtPlugin ~ ctx:", ctx)
  return {
      provide: {
        repositories: createRepository(axios)
      }
    }
  })