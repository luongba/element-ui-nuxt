import axios from "axios"

export default defineNuxtPlugin((ctx) => {
    return {
      provide: {
        axios
      }
    }
  })